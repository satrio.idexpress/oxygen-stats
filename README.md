# Oxygen Stats

A simple dashboard that display common component usage accross Oxygen Group front-end web repositories.

## Setup & Start Dashboard
```bash
yarn && yarn dev
```

## Generate Data
To generate component usage data, you can run
```bash
yarn generate-data
```

If you find any issue feel free to open an issue. Any further question please reach out `satrio.yamanda@idexpress.com`