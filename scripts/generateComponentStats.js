import { readFileSync } from "fs";
import { globSync } from "glob";
import path from "path";

export default function generateComponentStats({
  sourcePath,
  importPath,
  repoName,
  filter
}) {
  const componentStats = {};
  const globFormat = `${sourcePath.local}/src/**/*.tsx`;
  const files = globSync(globFormat);

  for (const file of files) {
    const localPath = path.join(
      repoName,
      path.relative(sourcePath.local, file)
    );
    const remotePath = path.join(
      sourcePath.remote,
      path.relative(sourcePath.local, file)
    );
    const relativePath = { local: localPath, remote: remotePath };
    const data = readFileSync(file);
    const dataString = data.toString();
    const importPattern = new RegExp(
      `import\\s(.+)\\sfrom\\s['"](${importPath}(.+)?)['"];?`,
      "ig"
    );

    if (dataString.search(importPattern) > -1) {
      const matches = importPattern.exec(dataString);
      if (!matches) return;
      const components = matches[1]
        .replace(/[\{\}\s]/g, "")
        .trim()
        .split(",");

      components.forEach((component) => {
        /* Filter non component import */
        if (component.search(new RegExp(filter.join('|'), "g")) > -1) return;
        if (componentStats[component]) {
          componentStats[component].usageCount += 1;
          componentStats[component].usagePaths.push(relativePath);
          componentStats[component].sourceRepo = repoName;
          if (!componentStats[component].importPaths.includes(matches[2])) {
            componentStats[component].importPaths.push(matches[2]);
          }
        } else {
          componentStats[component] = {
            usagePaths: [relativePath],
            usageCount: 1,
            importPaths: [matches[2]],
            sourceRepo: repoName,
          };
        }
      });
    }
  }

  return componentStats;
}
