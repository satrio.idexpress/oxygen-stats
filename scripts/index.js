import { writeFile } from "fs";
import path from "path";
import generateComponentStats from "./generateComponentStats.js";
import generateCsv from "./generateCsv.js";
import mergeObjects from "./mergeObjects.js";
import { config } from "../generator-config.js";

const { sources, outputDir, importPaths, filter } = config;

const allComponents = sources.reduce((finalComponents, sourcePath) => {
  const repoName = path.basename(sourcePath.local);

  const allObjects = importPaths.map((importPath) =>
    generateComponentStats({
      sourcePath,
      importPath,
      repoName,
      filter,
    })
  );
  const mergedObjects = mergeObjects([finalComponents, ...allObjects]);
  return mergedObjects;
}, {});

writeFile(
  `${outputDir}/stats.json`,
  JSON.stringify(allComponents),
  { encoding: "utf-8" },
  (err) => {
    if (err) throw err;
  }
);

generateCsv(allComponents, 'all');

sources.forEach((sourcePath) => {
  const repoName = path.basename(sourcePath.local);
  const stats = generateComponentStats({
    sourcePath,
    importPath: "antd",
    repoName,
    filter
  });
  const jsonObj = JSON.stringify(stats);

  writeFile(
    `${outputDir}/stats-${repoName}.json`,
    jsonObj,
    { encoding: "utf-8" },
    (err) => {
      if (err) throw err;
    }
  );

  generateCsv(stats, repoName);
});
