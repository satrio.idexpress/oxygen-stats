export default function mergeObjects(allObjects) {
  return allObjects.reduce((finalObject, currentObject) => {
    for (let key in currentObject) {
      if (finalObject[key] === undefined) {
        finalObject[key] = currentObject[key];

        for (let innerKey in finalObject[key]) {
          if (typeof currentObject[key][innerKey] === "string") {
            finalObject[key][innerKey] = [currentObject[key][innerKey]];
          } else {
            finalObject[key][innerKey] = currentObject[key][innerKey];
          }
        }
      } else {
        for (let innerKey in finalObject[key]) {
          switch (true) {
            case typeof finalObject[key][innerKey] === "number":
              finalObject[key][innerKey] += currentObject[key][innerKey];
              break;
            case Array.isArray(finalObject[key][innerKey]):
              const newItem = currentObject[key][innerKey];
              const theItems = finalObject[key][innerKey];
              if (Array.isArray(newItem)) {
                newItem.forEach(item => {
                  if (!theItems.includes(item)) {
                    theItems.push(item);
                  }
                });
              } else if (!theItems.includes(newItem)) {
                finalObject[key][innerKey].push(newItem);
              }
              break;
            default:
              finalObject[key][innerKey] = currentObject[key][innerKey];
          }
        }
      }
    }

    return finalObject;
  }, {});
}
