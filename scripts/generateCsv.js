import path from "path";
import { writeFile } from "fs";

export default function generateCsv(statsData, name) {
  const csvHeader = "Component Name, Usage, Import Path, Repo, Usage Paths";
  const csvFormat = Object.keys(statsData)
    .map((column) => {
      const name = column;
      const usage = statsData[column]["usageCount"];
      const importPath = statsData[column]["importPaths"].join(",");
      const sourceRepo = statsData[column]["sourceRepo"];
      const paths = statsData[column]["usagePaths"].join("\n, , , ,");

      return [name, usage, importPath, sourceRepo, paths].join("|");
    })
    .sort()
    .join("\n");

  writeFile(
    path.resolve(process.cwd(), `src/data/stats-${name}.csv`),
    [csvHeader, csvFormat].join("\n"),
    { encoding: "utf-8" },
    (err) => {
      if (err) throw err;
    }
  );
}
