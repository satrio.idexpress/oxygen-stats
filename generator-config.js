import path from "path";

const baseRepoUrl = "https://gitlab.com/projectaltair";

export const config = {
  sources: [
    {
      local: path.resolve(process.cwd(), "../oms"),
      remote: `${baseRepoUrl}/ide/idexpress/web/oms/-/blob/develop`,
    },
    {
      local: path.resolve(process.cwd(), "../vip-portal-web-mirror"),
      remote: `${baseRepoUrl}/ide/idexpress/web/vip-portal-web-mirror/-/blob/main`,
    },
    {
      local: path.resolve(process.cwd(), "../seller-portal-web"),
      remote: `${baseRepoUrl}/gpn/endor/Frontend/web/seller-portal-web/-/blob/main`,
    }
  ],
  importPaths: ["antd", "src/components/common"],
  outputDir: "src/data",
  filter: ["type","Event","Param","Props","Instance","Style", "^do"]
};
