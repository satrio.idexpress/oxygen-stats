import data from "../data/stats.json";

export type StatsData = typeof data;
export const statsData = data;
export const componentNames = Object.keys(statsData).sort();