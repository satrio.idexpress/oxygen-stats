import {
  Accordion,
  Badge,
  Container,
  ScrollArea,
  Table,
  Text,
} from "@mantine/core";
import Overview from "./components/home/Overview";
import { componentNames, statsData } from "./common/constants";

function App() {
  return (
    <Container fluid>
      <Text size={32} mb={0} mt={16}>
        Common Component Usage
      </Text>
      <Text size={18} mb={24}>
        OMS, VIP Portal, Seller Portal
      </Text>
      <Overview />
      <ScrollArea>
        <Table verticalSpacing="lg" p={16} sx={{ tableLayout: "fixed" }}>
          <colgroup>
            <col span={1} style={{ width: 300 }} />
            <col span={1} style={{ width: 200 }} />
            <col span={1} style={{ width: 200 }} />
            <col span={1} style={{ width: 400 }} />
            <col span={1} style={{ width: 600 }} />
          </colgroup>
          <thead style={{ fontWeight: "bold", position: "sticky" }}>
            <tr>
              <td>Component Name</td>
              <td>Usage Count</td>
              <td>Repo</td>
              <td>Import Paths</td>
              <td>Usage Paths</td>
            </tr>
          </thead>
          <tbody>
            {componentNames.map((component) => {
              /* @ts-ignore */
              const componentInfo = statsData[component];
              return (
                <tr key={component} style={{ verticalAlign: "top" }}>
                  <td>{component}</td>
                  <td>{componentInfo.usageCount}</td>
                  <td>
                    {componentInfo.sourceRepo.map((repo: string) => (
                      <Badge key={repo}>{repo}</Badge>
                    ))}
                  </td>
                  <td>
                    {componentInfo.importPaths
                      .sort()
                      .map((path: string, index: number) => (
                        <div key={`${path}-${index}`}>{path}</div>
                      ))}
                  </td>
                  <td>
                    <Accordion>
                      <Accordion.Item value="default">
                        <Accordion.Control>See Paths</Accordion.Control>
                        <Accordion.Panel>
                          <div style={{ overflow: "auto" }}>
                            {componentInfo.usagePaths.map(
                              (
                                path: { local: string; remote: string },
                                index: number
                              ) => (
                                <div
                                  key={`${path}-${index}`}
                                  style={{ whiteSpace: "nowrap" }}
                                >
                                  <a
                                    href={path.remote}
                                    target="_blank"
                                    rel="nofollow noopener"
                                  >
                                    {path.local}
                                  </a>
                                </div>
                              )
                            )}
                          </div>
                        </Accordion.Panel>
                      </Accordion.Item>
                    </Accordion>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </ScrollArea>
    </Container>
  );
}

export default App;
