import { Badge, Box, Card, Flex, Text, Tooltip } from "@mantine/core";
import { AiOutlineBulb } from "react-icons/ai";
import { componentNames, statsData } from "../../../common/constants";

const mostUsed = componentNames.reduce(
  (finalComponent, currentName) => {
    /* @ts-ignore */
    const currentComponent = statsData[currentName];
    if (currentComponent.usageCount > finalComponent.usageCount) {
      return { name: currentName, usageCount: currentComponent.usageCount };
    }
    return finalComponent;
  },
  { usageCount: 0, name: "" }
);
const commonlyUsed: any[] = componentNames.filter(
  /* @ts-ignore */
  (name) => statsData[name].sourceRepo.length > 1
);

export default function Overview() {
  return (
    <Flex gap={8} mb={40}>
      <Card shadow={"sm"} padding="lg" withBorder sx={{ flexShrink: 0 }}>
        <Flex direction="column" h="100%" justify={"space-between"}>
          <Text size={40} mb="sm" sx={{ lineHeight: 1 }}>
            {componentNames.length}
          </Text>
          <Text size={14} mb={4} color="gray">
            Total Components
          </Text>
        </Flex>
      </Card>
      <Card shadow={"sm"} padding="lg" withBorder sx={{ flexShrink: 0 }}>
        <Flex direction="column" h="100%" justify={"space-between"}>
          <Text size={40} mb="sm" sx={{ lineHeight: 1 }}>
            {mostUsed.name}
          </Text>
          <Flex align="center" gap={4} mb={4}>
            <Text size={14} color="gray">
              Most Used Component
            </Text>
            <Tooltip label={`${mostUsed.usageCount} usage`}>
              <Box pos="absolute" top={8} right={8}>
                <AiOutlineBulb />
              </Box>
            </Tooltip>
          </Flex>
        </Flex>
      </Card>
      <Card shadow={"sm"} padding="lg" withBorder maw={800}>
        <Flex h="100%" direction={"column"} justify={"space-between"}>
          <Box mb={4}>
            {commonlyUsed.map((component) => (
              <Badge mr={4} color="green">
                {component}
              </Badge>
            ))}
          </Box>
          <Text size={14} color="gray" mb={4}>
            Used in Many Repo
          </Text>
        </Flex>
      </Card>
    </Flex>
  );
}
